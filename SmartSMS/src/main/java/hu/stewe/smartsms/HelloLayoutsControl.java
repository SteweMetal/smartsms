/*
Copyright (c) 2011, Sony Ericsson Mobile Communications AB
Copyright (c) 2011-2013, Sony Mobile Communications AB

 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 * Neither the name of the Sony Ericsson Mobile Communications AB / Sony Mobile
 Communications AB nor the names of its contributors may be used to endorse or promote
 products derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package hu.stewe.smartsms;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;

import com.sonyericsson.extras.liveware.aef.control.Control;
import com.sonyericsson.extras.liveware.aef.registration.Registration;
import com.sonyericsson.extras.liveware.extension.util.control.ControlExtension;
import com.sonyericsson.extras.liveware.extension.util.control.ControlListItem;
import com.sonyericsson.extras.liveware.extension.util.control.ControlObjectClickEvent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This demonstrates two different approaches, bitmap and layout, for displaying
 * a UI. The bitmap approach is useful for accessories without layout support,
 * e.g. SmartWatch.
 * This sample shows all UI components that can be used, except Gallery and
 * ListView.
 */
class HelloLayoutsControl extends ControlExtension {

    enum SCREEN {
        MAIN, SMS_THREAD_LIST, SMS_LIST
    }

    /**
     * Contains the chosen UI to render, e.g. layout or bitmap.
     */
    private SCREEN screen = SCREEN.MAIN;

    /**
     * Used to toggle if an icon will be visible in the bitmap UI.
     */
    private boolean mIconImage = true;

    List<SMS> smsList = new ArrayList<SMS>();
    List<SMSThread> smsThreadList = new ArrayList<SMSThread>();
    int currentSMSThread = -1;

    /**
     * Create control extension.
     *
     * @param hostAppPackageName Package name of host application.
     * @param context            The context.
     * @param handler            The handler to use.
     */
    HelloLayoutsControl(final String hostAppPackageName, final Context context, Handler handler) {
        super(context, hostAppPackageName);
        if (handler == null) {
            throw new IllegalArgumentException("handler == null");
        }
    }

    /**
     * Return the width of the screen which this control extension supports.
     *
     * @param context The context.
     * @return The width in pixels.
     */
    public static int getSupportedControlWidth(Context context) {
        return context.getResources().getDimensionPixelSize(R.dimen.smart_watch_2_control_width);
    }

    /**
     * Return the height of the screen which this control extension supports.
     *
     * @param context The context.
     * @return The height in pixels.
     */
    public static int getSupportedControlHeight(Context context) {
        return context.getResources().getDimensionPixelSize(R.dimen.smart_watch_2_control_height);
    }

    @Override
    public void onDestroy() {
        Log.d(HelloLayoutsExtensionService.LOG_TAG, "onDestroy: HelloLayoutsControl");
    }

    @Override
    public void onObjectClick(final ControlObjectClickEvent event) {
        Log.d(HelloLayoutsExtensionService.LOG_TAG,
                "onObjectClick: HelloLayoutsControl click type: " + event.getClickType());


        // Check which view was clicked and then take the desired action.
        switch (event.getLayoutReference()) {
            case R.id.btn_show_sms:
                if (screen == SCREEN.MAIN)
                    screen = switchLayout();
                updateLayout();
                break;
            case R.id.btn_update_layout:
                updateLayout();
                break;
        }
    }

    private SCREEN switchLayout() {
        if (screen == SCREEN.MAIN)
            return SCREEN.SMS_THREAD_LIST;
        else
            return SCREEN.MAIN;
    }

    @Override
    public void onListItemClick(ControlListItem listItem, int clickType, int itemLayoutReference) {
        int itemId = listItem.listItemId;
        currentSMSThread = smsThreadList.get(itemId).threadId;
        screen = SCREEN.SMS_LIST;
        updateLayout();
    }

    /**
     * This is an example of how to update the entire layout and some of the
     * views. For each view, a bundle is used. This bundle must have the layout
     * reference, i.e. the view ID and the content to be used. This method
     * updates an ImageView and a TextView.
     *
     * @see Control.Intents#EXTRA_DATA_XML_LAYOUT
     * @see Registration.LayoutSupport
     */
    private void updateLayout() {

        switch (screen) {
            case MAIN:
                updateMainScreen();
                break;
            case SMS_LIST:
                updateSMSListScreen();
                break;
            case SMS_THREAD_LIST:
                updateSMSThreadScreen();
                break;
            default:
                break;
        }
    }

    private void updateMainScreen() {
        Bundle bundle = new Bundle();
        bundle.putInt(Control.Intents.EXTRA_LAYOUT_REFERENCE, R.id.battery_percent);
        String batteryPercent = Float.toString(getBatteryLevel());
        bundle.putString(Control.Intents.EXTRA_TEXT, batteryPercent + "%");

        Bundle[] bundleData = new Bundle[1];
        bundleData[0] = bundle;

        showLayout(R.layout.main_layout, bundleData);
    }

    private void updateSMSListScreen() {
        getSMSListFrom(currentSMSThread);
        showLayout(R.layout.sms_list_layout, null);
        sendListCount(R.id.sms_list, smsList.size());
    }

    private void updateSMSThreadScreen() {
        getSMSThreads();
        showLayout(R.layout.sms_list_layout, null);
        sendListCount(R.id.sms_list, smsThreadList.size());
    }

    private float getBatteryLevel() {
        Intent batteryIntent = mContext.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            return 50.0f;
        }

        return ((float) level / (float) scale) * 100.0f;
    }

    private void getUnreadSMSList() {
        ContentResolver cr = mContext.getContentResolver();
        Uri uriSMSURI = Uri.parse("content://sms/inbox");

        Cursor cur = cr.query(uriSMSURI, null, "read = 0", null, null);

        smsList.clear();
        if (cur.moveToFirst()) {
            do {
                smsList.add(getSMSFromCursor(cur));

                cur.moveToNext();
            } while (!cur.isAfterLast());
        }
    }

    private void getSMSListFrom(int threadId) {
        final String SMS_URI_INBOX = "content://sms/inbox";
        Uri uri = Uri.parse(SMS_URI_INBOX);
        String[] projection = new String[]{"_id", "address", "person", "body", "date", "type"};
        Cursor cur = mContext.getContentResolver().query(uri, projection, "thread_id=" + threadId, null, "date desc");

        smsList.clear();
        if (cur.moveToFirst()) {
            do {
                smsList.add(getSMSFromCursor(cur));

                cur.moveToNext();
            } while (!cur.isAfterLast());
        }
        cur.close();
    }

    private void getSMSListFrom(String sender) {
        final String SMS_URI_INBOX = "content://sms/inbox";
        Uri uri = Uri.parse(SMS_URI_INBOX);
        String[] projection = new String[]{"_id", "address", "person", "body", "date", "type"};
        Cursor cur = mContext.getContentResolver().query(uri, projection, "address='" + sender + "'", null, "date desc");

        smsList.clear();
        if (cur.moveToFirst()) {
            do {
                smsList.add(getSMSFromCursor(cur));

                cur.moveToNext();
            } while (!cur.isAfterLast());
        }
        cur.close();
    }

    private void getSMSThreads() {
        ContentResolver cr = mContext.getContentResolver();
        Uri smsUri = Uri.parse("content://mms-sms/conversations/");

        Cursor cur = cr.query(smsUri, null, null, null, "date DESC");

        smsThreadList.clear();
        if (cur.moveToFirst()) {
            do {
                smsThreadList.add(getSMSThreadFromCursor(cur));

                cur.moveToNext();
            } while (!cur.isAfterLast());
        }
        cur.close();
    }

    private SMS getSMSFromCursor(Cursor cur) {
        String address = cur.getString(cur.getColumnIndex("address"));
        int threadId = 0;//cur.getInt(cur.getColumnIndex("thread_id"));
        int person = cur.getInt(cur.getColumnIndex("address"));
        String body = cur.getString(cur.getColumnIndex("body"));
        long date = cur.getLong(cur.getColumnIndex("date"));
        int type = cur.getInt(cur.getColumnIndex("type"));

        SMS sms = new SMS(address, threadId, person, body, date, type);
        return sms;
    }

    private SMSThread getSMSThreadFromCursor(Cursor cur) {
        StringBuffer buffer = new StringBuffer();
        for (String col : cur.getColumnNames())
            buffer.append(col).append(" ");
        Log.d("THREAD COLUMN NAMES", buffer.toString());

        String person = cur.getString(cur.getColumnIndexOrThrow("person"));
        String address = cur.getString(cur.getColumnIndexOrThrow("address"));
        address = getContactName(address);

        int threadId = cur.getInt(cur.getColumnIndex("thread_id"));
        long date = cur.getLong(cur.getColumnIndex("date"));
        int messageCount = 0;

        SMSThread smsThread = new SMSThread(address, date, messageCount, threadId);
        return smsThread;
    }

    private String getContactName(String phoneNumber) {
        ContentResolver cr = mContext.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor contactCursor = cr.query(uri,
                null, null, null, null);

        try {
            if (contactCursor != null && contactCursor.getCount() > 0) {
                contactCursor.moveToNext();
                return contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
            } else {
                return phoneNumber;
            }
        } finally {
            if (contactCursor != null) {
                contactCursor.close();
            }
        }
    }


    @Override
    public void onRequestListItem(int layoutReference, int listItemPosition) {
        switch (screen) {
            case SMS_THREAD_LIST:
                onRequestSMSThreadListItem(layoutReference, listItemPosition);
                break;
            case SMS_LIST:
                onRequestSMSListItem(layoutReference, listItemPosition);
                break;
        }
    }

    private void onRequestSMSThreadListItem(int layoutReference, int listItemPosition) {
        ControlListItem item = new ControlListItem();
        item.layoutReference = layoutReference;
        item.dataXmlLayout = R.layout.sms_thread_list_item;
        item.listItemId = listItemPosition;
        item.listItemPosition = listItemPosition;
        item.layoutData = new Bundle[2];
        item.layoutData[0] = new Bundle();
        Bundle data = item.layoutData[0];
        data.putInt(Control.Intents.EXTRA_LAYOUT_REFERENCE, R.id.sms_thread_partner);
        data.putString(Control.Intents.EXTRA_TEXT, smsThreadList.get(listItemPosition).address);
        item.layoutData[1] = new Bundle();
        data = item.layoutData[1];
        data.putInt(Control.Intents.EXTRA_LAYOUT_REFERENCE, R.id.sms_thread_date);

        Date date = new Date(smsThreadList.get(listItemPosition).date);
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        data.putString(Control.Intents.EXTRA_TEXT, dateFormatter.format(date));
        sendListItem(item);
    }

    private void onRequestSMSListItem(int layoutReference, int listItemPosition) {
        ControlListItem item = new ControlListItem();
        item.layoutReference = layoutReference;
        item.dataXmlLayout = R.layout.sms_list_item;
        item.listItemId = listItemPosition;
        item.listItemPosition = listItemPosition;
        item.layoutData = new Bundle[2];
        item.layoutData[0] = new Bundle();
        Bundle data = item.layoutData[0];
        data.putInt(Control.Intents.EXTRA_LAYOUT_REFERENCE, R.id.sms_item_row1);
        data.putString(Control.Intents.EXTRA_TEXT, smsList.get(listItemPosition).body);
        item.layoutData[1] = new Bundle();
        data = item.layoutData[1];
        data.putInt(Control.Intents.EXTRA_LAYOUT_REFERENCE, R.id.sms_item_row2);

        Date date = new Date(smsList.get(listItemPosition).date);
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        data.putString(Control.Intents.EXTRA_TEXT, dateFormatter.format(date));
        sendListItem(item);
    }

    @Override
    public void onResume() {
        // Send a UI when the extension becomes visible.
        updateLayout();
        super.onResume();
    }

    /**
     * This method toggles what icon to show.
     */
    private void toggleImage() {
        Log.d(HelloLayoutsExtensionService.LOG_TAG, "toggleImage: HelloLayoutsControl");

        // The sendImage method is used to update an image of a single ImageView.
        if (mIconImage) {
            sendImage(R.id.image, R.drawable.actions_view_in_phone);
        } else {
            sendImage(R.id.image, R.drawable.icon_extension48);
        }
        mIconImage = !mIconImage;
    }
}
