package hu.stewe.smartsms;

/**
 * Created by juhos on 2015.12.14..
 */
public class SMS {
    public String address;
    public String body;
    public int threadId;
    public int person;
    public long date;
    public int type;

    public SMS(String address, int threadId, int person, String body, long date, int type) {
        this.address = address;
        this.threadId = threadId;
        this.person = person;
        this.body = body;
        this.date = date;
        this.type = type;
    }


}
