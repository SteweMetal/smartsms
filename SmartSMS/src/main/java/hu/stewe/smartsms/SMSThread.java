package hu.stewe.smartsms;

/**
 * Created by juhos on 2015.12.15..
 */
public class SMSThread {
    public String address;
    public long date;
    public int messageCount;
    public int threadId;

    public SMSThread(String address, long date, int messageCount, int threadId) {
        this.address = address;
        this.date = date;
        this.messageCount = messageCount;
        this.threadId = threadId;
    }
}
